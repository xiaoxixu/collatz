#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    assert i>=1
    assert j>=1
    assert i<1000000
    assert j<1000000
    def cycle_length (n) :
        assert n > 0
        if n in cache_dict:
            return cache_dict[n]
        c = 1
        while n > 1 :
            if (n % 2) == 0 :
                n = (n // 2)
            else :
                n = (3 * n) + 1
            c += 1
        assert c > 0
        cache_dict[n] = c
        return c
    cache_dict = {} # implement lazy cache
    max_cycle = 0
    if i == j: # check three conditions: i=j, i>j, i<j
        assert cycle_length(i) <= 525
        return cycle_length(i)
    elif i < j:
        while i != j:
            if cycle_length(i) > max_cycle:
                max_cycle = cycle_length(i)
            i += 1
        assert max_cycle <= 525
        return max_cycle
    else: 
        while j != i:
            if cycle_length(j) > max_cycle:
                max_cycle = cycle_length(j)
            j += 1
        assert max_cycle <= 525
        return max_cycle
        

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
